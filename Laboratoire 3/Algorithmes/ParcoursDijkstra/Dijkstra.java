package ParcoursDijkstra;
	// A Java program for Dijkstra's single source shortest path algorithm.
	// The program is for adjacency matrix representation of the graph
	import java.util.*;


import Main.*;
import Noeud.*;
	import java.lang.*;
	import java.io.*;
	 //source:http://www.geeksforgeeks.org/greedy-algorithms-set-6-dijkstras-shortest-path-algorithm/
	public class Dijkstra
	{
	    // A utility function to find the vertex with minimum distance value,
	    // from the set of vertices not yet included in shortest path tree
	    static public  List<Integer> chemin = new ArrayList<Integer>();
	    static int minDistance(int dist[], Boolean sptSet[], int N)
	    {
	        // Initialize min value
	        int min = Integer.MAX_VALUE, min_index=-1;
	 
	        for (int v = 0; v < N; v++)
	            if (sptSet[v] == false && dist[v] <= min)
	            {
	                min = dist[v];
	                min_index = v;
	                chemin.add(v);
	            }
	 
	        return min_index;
	    }
	 
	    // A utility function to print the constructed distance array
	    static void printSolution(int dist[], int n, int N)
	    {
	        System.out.println("Noeud   Distance from Source");
	        for (int i = 0; i < N; i++)
	        	if(dist[i] <100000)
	        		System.out.println(i+" \t\t "+dist[i]);
	        	else
	        		 System.out.println(i+" \t\t -1");
	    }
	 
	    // Funtion that implements Dijkstra's single source shortest path
	    // algorithm for a graph represented using adjacency matrix
	    // representation
	    public static void DijkstraOptimize(int[][] graph,int src, int N, int Dest)
	    {
	        int dist[] = new int[N]; // The output array. dist[i] will hold
	                                 // the shortest distance from src to i
	 
	        // sptSet[i] will true if vertex i is included in shortest
	        // path tree or shortest distance from src to i is finalized
	        Boolean sptSet[] = new Boolean[N];
	 
	        // Initialize all distances as INFINITE and stpSet[] as false
	        for (int i = 0; i < N; i++)
	        {
	            dist[i] = Integer.MAX_VALUE;
	            sptSet[i] = false;
	        }
	       
	        // Distance of source vertex from itself is always 0
	        dist[src] = 0;
	 
	        // Find shortest path for all vertices
	        for (int count = 0; count < N-1; count++)
	        {
	            // Pick the minimum distance vertex from the set of vertices
	            // not yet processed. u is always equal to src in first
	            // iteration.
	            int u = minDistance(dist, sptSet, N);
	           
	            // Mark the picked vertex as processed
	            sptSet[u] = true;
	 
	            // Update dist value of the adjacent vertices of the
	            // picked vertex.
	            for (int v = 0; v < N; v++)
	 
	                // Update dist[v] only if is not in sptSet, there is an
	                // edge from u to v, and total weight of path from src to
	                // v through u is smaller than current value of dist[v]
	                if (!sptSet[v] && graph[u][v]!=-1 &&
	                        dist[u] != Integer.MAX_VALUE &&
	                        dist[u]+graph[u][v] < dist[v])
	                {
	                    dist[v] = dist[u] + graph[u][v];
	                    //chemin.add(v);
	                }
        }
	            Main.Log("Chemin Visiter");
				for(int j=0;j<chemin.size();j++)
				{
					 Main.Log(chemin.get(j) + ";");
				}
				 Main.Log("\n");
	        // print the constructed distance array
	        printSolution(dist, N, N);
	    }
	    
	    
	    
	    
	    
	    
//	    public static void DijkstraOptimize(listeNode Listadjacant, String aSource ,String  aDestination, int N)
//	    {
////	    	long tStart = System.currentTimeMillis();
//	    	
//	    	List<String> chemin = new ArrayList<String>();
//	    	
//	    	int valeurTotal = 0;
//	    	int i = 0;
//	    	int[] temp = new int[2];
//	    	int source = 0;
//	    	int destination = 0;
//	    	int nbvisite =0;
//	    	source = Integer.parseInt(aSource); 
//	    	destination = Integer.parseInt(aDestination); 
//	    	Node CurrentNode = Listadjacant.arrayNode().get(source);
//	    	temp[1] = source;
//	    	Node EndNode = Listadjacant.arrayNode().get(destination);
//	    	while(nbvisite <= N) 
//	    	{
//	    		//temp = MinDist(Listadjacant, temp[1]);
//	    		chemin.add(Listadjacant.arrayNode().get(temp[1]).getName());
//	    		valeurTotal = valeurTotal + temp[0];
//	    		
//	    		nbvisite = nbvisite+1;
//	    	}
//	    	Main.Log("Chemin Visiter");
//			for(int j=0;j<chemin.size();j++)
//			{
//				 Main.Log(chemin.get(j) + ";");
//			}
//			
//	    	// Main.Log("Temp de calcule est de :" +valTemps + "\n");
//	    	 Main.Log("Distance est de :"+valeurTotal + "\n");
//	    		
//	    }
//		
//	    static int[] MinDist(listeNode aNodes, int aIndex)
//	    {
//	    	int Distance = 0; int NextSummit = 0;
//	    	Node Instance = aNodes.arrayNode().get(aIndex);
//	    	ArrayList index = Instance.getIndexVoisins();
//	    	ArrayList value = Instance.getPoidsLiens();
//	    	
//	        int[] ReturnValue = new int[2];
//	        NextSummit = (int) index.get();
//	        Distance = (int) value.get(0);
//	        
// 	        for (int i = 0; i < index.size(); i++)
//	        {
//	        	int tempindex =(int) index.get(i);
//	        	int tempvalue = (int) value.get(i);
//	        	if(tempvalue < Distance)
//	        	{
//	        		if(aNodes.arrayNode().get(tempindex).getNVoisins() !=1)
//	        		{
//	        			if(aNodes.arrayNode().get(i).getVisite() != true)
//	        			{
//		        			Distance = tempvalue;
//		        			NextSummit = tempindex;
//	        			}
//	        		}
//	        		
//	        	}
//	        }
// 	        ReturnValue[0] = Distance;
// 	        ReturnValue[1] = NextSummit;
//	        return ReturnValue;
//	    }
  
	}    
	