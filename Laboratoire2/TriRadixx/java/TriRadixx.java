// Radix sort Java implementation
// Reference : http://quiz.geeksforgeeks.org/Radixx/
package TriRadixx.java;
import Stats.java.Stats; 
import java.util.List;
import java.util.ArrayList;
import java.util.*;


public class TriRadixx {

    // A utility function to get maximum value in arr[]
    static int getMax(int arr[], int n)
    {
    	Stats.barometre(0);
        int mx = arr[0];
        for (int i = 1; i < n; i++){
        	Stats.barometre(1);
            if (arr[i] > mx){
            	Stats.barometre(2);
                mx = arr[i];
            }
        }
        return mx;
    }

    // A function to do counting sort of arr[] according to
    // the digit represented by exp.
    static void countSort(int arr[], int n, int exp)
    {
    	Stats.barometre(3);
    	int output[] = new int[n]; // output array
        int i;
        int count[] = new int[10];
        Arrays.fill(count,0);

        // Store count of occurrences in count[]
        for (i = 0; i < n; i++){
        	Stats.barometre(4);
            count[ (arr[i]/exp)%10 ]++;
        }

        // Change count[i] so that count[i] now contains
        // actual position of this digit in output[]
        for (i = 1; i < 10; i++){
        	Stats.barometre(5);
            count[i] += count[i - 1];
            }

        // Build the output array
        for (i = n - 1; i >= 0; i--)
        {
        	Stats.barometre(6);
            output[count[ (arr[i]/exp)%10 ] - 1] = arr[i];
            count[ (arr[i]/exp)%10 ]--;
        }

        // Copy the output array to arr[], so that arr[] now
        // contains sorted numbers according to curent digit
        for (i = 0; i < n; i++){
        	Stats.barometre(7);
        	arr[i] = output[i];
        }
    }

    // The main function to that sorts arr[] of size n using
    // Radix Sort
    public void sortxx(int arr[], int n)
    {
        // Find the maximum number to know number of digits
    	Stats.barometre(8);
        int m = getMax(arr, n);

        // Do counting sort for every digit. Note that instead
        // of passing digit number, exp is passed. exp is 10^i
        // where i is current digit number
        for (int exp = 1; m/exp > 0; exp *= 10){
        	Stats.barometre(9);
        	countSort(arr, n, exp);
        }
    }

    /*Driver function to check for above function*/
    public static  List <Integer> radixsort (List<Integer> aArraylist)
    {
    	Stats.barometre(10);
    	int []arr = new int [aArraylist.size()];
        
    	for(int i = 0 ; i < arr.length;i++)
    		{
        	Stats.barometre(11);
    	    arr[i] = aArraylist.get(i);
    		}
    	
        TriRadixx ob = new TriRadixx();
        ob.sortxx(arr,arr.length);
        
        List<Integer> Returned_List = new ArrayList<Integer>();
        
        for(int j = 0; j<aArraylist.size(); j++)
		{
        	Stats.barometre(12);
			Returned_List.add(arr[j]);
		}

		return  Returned_List;
    }
}
/* This code is contributed by Devesh Agrawal */