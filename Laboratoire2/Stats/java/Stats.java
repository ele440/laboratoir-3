package Stats.java;

import java.util.*;

import Controleur.java.*;
import Generation.java.generation;

import java.io.*;
import java.math.*;
import java.time.*;
public class Stats
{
	private static int[] counter = new int[20];
	private static int mode = 1;

	public static int Iteration_Counter = -1;
	
	public static void barometre(int n)	{
		counter[n]++;
	}
	//D�clartion de l'objet Search_Results
	 public static class Search_Results
	{
		 public static int N;
		 public static int R;
		 public static int D;
		 public static String SearchName;
		 public static String TriName;
		public static long StartTime;
		public static long EndTime;
		public static long Tp;
		public static long Tr;
		public static long Temp_Calcule;
		public static long Ta;
		public static int K;
		public static int Found_values;
		public static List<Integer> Iteration = new ArrayList<Integer>();
		public static List<Integer> All_Data = new ArrayList<Integer>();
		public static List<Integer> Searched_Values = new ArrayList<Integer>();
		
	}
	 //D�but du minuteur pour le calcule de performance
	 public static void Start_Time()
	 {
		 Search_Results.StartTime = System.currentTimeMillis();

	 }
	 //Fin du minuteur pour le clacule de performance
	 public static void End_Time()
	 {
		 Search_Results.EndTime = System.currentTimeMillis();	
	 }
	 //Fonction de calcule de Temps de recherche
	 public static void Calcule_TR()
	 {
		 Search_Results.Tr = Search_Results.EndTime-Search_Results.StartTime;
		 Search_Results.EndTime=0;
		 Search_Results.StartTime=0;
	 }
	 //Fonction de calcule du temps de pr�paration
	 public static void Calcule_TP()
	 {
		 Search_Results.Tp = Search_Results.EndTime-Search_Results.StartTime;
		 Search_Results.EndTime=0;
		 Search_Results.StartTime=0;
	 }
	 //Fonction de calcule qui calcule le temps ammortie
	 public static void Calcule_TA()
	 { 
		 Search_Results.Ta = Search_Results.Tp / Search_Results.K +Search_Results.Tr;
	 }
	 //Fonction qui calcule le temp de calcule total
	 public static void Calcule_TCalcul()
	 { 
		 Search_Results.Temp_Calcule = Search_Results.Tp + Search_Results.K * Search_Results.Tr;
	 }
	//Fonction qui affiche les r�sultats obetnue dans la console
	 public static void Get_Search_Results()
	 {
		 	Calcule_TA();
		 	Calcule_TCalcul();
		 	
		 	generation.log("\nAlgorithm de recherche :"+ Search_Results.SearchName+ "\n");
		 	generation.log("Algorithm de tri :"+ Search_Results.TriName+ "\n");
		 	generation.log("N :"+ Search_Results.N+ " R :"+ Search_Results.R+ " D :"+ Search_Results.D + " K :"+ Search_Results.K + "\n");
		 	generation.log("Tp :"+ Search_Results.Tp+ "\n");
		 	generation.log("Tr :"+ Search_Results.Tr+ "\n");
		 	generation.log("Ta :"+ Search_Results.Ta+ "\n");
		 	generation.log("T :"+ Search_Results.Temp_Calcule+ "\n");
		 	generation.log("\nNombre d'element trouver: " + Search_Results.Found_values);
		 	generation.log("\nValeurs rechercher: \n");
			for(int i = 0;i<Stats.Search_Results.Searched_Values.size();i++)
			{
				generation.log(Stats.Search_Results.Searched_Values.get(i)+", ");
			}
			generation.log("\nNombre d'iterations: \n");
			for(int i = 0;i<Stats.Search_Results.Iteration.size();i++)
			{
				generation.log(Stats.Search_Results.Iteration.get(i)+", ");
			}
			generation.log("\nToutes la liste des element trier ou non : \n");
			for(int i = 0;i<Stats.Search_Results.All_Data.size();i++)
			{
				
				generation.log(Stats.Search_Results.All_Data.get(i)+", ");
			}
			generation.log("\n");
	 }
	 //Fonction qui sert a reinitialiser les valeurs utiliser lors du trie et de la recherche
	 public static void Reset_Search_Results()
	 {
		 	Search_Results.Tp = 0;
		 	Search_Results.Tr = 0;
		 	Search_Results.Ta = 0;
		 	Search_Results.Temp_Calcule = 0;
		 	Search_Results.Found_values = 0;
		 	Search_Results.All_Data.clear();
			Search_Results.Searched_Values.clear();
			Search_Results.Iteration.clear();
	 }
	 //Fonction qui sert a calculer le maximum Barom�trique
	public static int max(int[] cnt) {
		int max = 0;
		
		for(int i=0; i<20; i++) {
			if(cnt[i]>max) {
				max = cnt[i];
			}
		}
		return max;
	}
	//Fonction qui sert � r�initialiser le compteur de la valeur maximal Barom�trique.
	public static int resBarometre() {
		return(max(counter));
	}
	//Fonction qui sert a sauver les r�sultats dans un fichier extern de type .txt
	// Le nom du fichier txt est Resultats_N_R_D_itt�ration
	public static void SauverStats(int[] NRD, String algo, int i) {
		
		PrintWriter FichierOUT = null;
		File theDir = new File("Resultats_"+algo);
		;
		
		try {
			if (!theDir.exists()) {
		        theDir.mkdir();
			}
			
			FichierOUT = new PrintWriter(theDir+"/"+i+".txt");		
			FichierOUT.println("\nAlgorithm de recherche :"+ Search_Results.SearchName+ "\n");
			FichierOUT.println("Algorithm de tri :"+ Search_Results.TriName+ "\n");
		 	FichierOUT.println("N :"+ Search_Results.N+ " R :"+ Search_Results.R+ " D :"+ Search_Results.D + " K :"+ Search_Results.K + "\n");
			FichierOUT.println("\nTp :"+ Search_Results.Tp+ "\n");
			FichierOUT.println("Tr :"+ Search_Results.Tr+ "\n");
			FichierOUT.println("Ta :"+ Search_Results.Ta+ "\n");
			FichierOUT.println("T :"+ Search_Results.Temp_Calcule+ "\n");
			FichierOUT.println("\nNombre d'element trouver: " + Search_Results.Found_values);
			FichierOUT.println("\nValeurs rechercher: \n");
			for(int j = 0;j<Stats.Search_Results.Searched_Values.size();j++)
			{
				FichierOUT.print(Stats.Search_Results.Searched_Values.get(j)+", ");
			}
			FichierOUT.println(" ");
			FichierOUT.println("\nNombre d'iterations: \n");
			for(int j = 0;j<Stats.Search_Results.Iteration.size();j++)
			{
				FichierOUT.print(Stats.Search_Results.Iteration.get(j)+", ");
			}
			FichierOUT.println(" ");
			FichierOUT.println("\nToutes la liste des element trier ou non : \n");
			for(int j = 0;j<Stats.Search_Results.All_Data.size();j++)
			{
				
				FichierOUT.print(Stats.Search_Results.All_Data.get(j)+", ");
			}
			FichierOUT.close();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}