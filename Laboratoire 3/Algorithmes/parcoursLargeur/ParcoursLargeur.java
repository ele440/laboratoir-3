package parcoursLargeur;
import Noeud.*;

import java.util.ArrayList;
import java.util.List;



public class ParcoursLargeur {
	
	private static void enfile(List<Integer> file, int element) {
		file.add(element);
	}
	
	private static int defile(ArrayList<Integer> file) {
		int element = 0;
		if(!file.isEmpty()) {
			element = file.get(0);
			file.remove(0);
		}
		return element;
	}
	
	
	public static Boolean Largeur(List<Node> A, List<Integer> Chemin, int Source, int Destination, double Temps) {
	
		//List<Node> A = new ArrayList<Node>();
		ArrayList<Integer> parcours = new ArrayList<Integer>();
		int noeudCourant, nomVoisin;
		
		long tStart = System.currentTimeMillis();
		enfile(parcours, Source);
		
		while(! parcours.isEmpty()) {
			noeudCourant = defile(parcours);
			Chemin.add(noeudCourant);
			A.get(noeudCourant).getVisite(); 
			for(int i = 0; i <  A.get(noeudCourant).getIndexVoisins().size(); i++) {
				nomVoisin = A.get(noeudCourant).getIndexVoisins().get(i);
				if(!A.get(nomVoisin).getVisite()) {
					enfile(parcours, nomVoisin);
					
				}
			}
		}
		long tEnd = System.currentTimeMillis();
		long tDelta = tEnd - tStart;
		Temps = tDelta / 1000.0;
		return true;
	}
	
}