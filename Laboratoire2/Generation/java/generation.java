package Generation.java;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import Stats.java.Stats;
import TriPigeonnier.java.*;


public class generation {	
	//Nom:Generator
	//Description:Fonction pricipale de g�n�ration de valeurs al�atoires
	//Arguments: Valeur du nombre d'�l�ment K
	//			 Valeur de la puissance R
	//			 Valeur du degr� de d�sordre D
	//Retourne: une liste de valeur al�atoire non tri�
	public static List<Integer> Generator(int aK, int aR, int aD)
	 {		 	
		 int N = 1000*aK;
		 List<Integer> Generated_Random_List = new ArrayList<Integer>();
		 Generated_Random_List = GenerateRandomArray(N,aR,aD);	
		 return Generated_Random_List;
	}
	//Nom:GenaterRandomSearchArray
	//Description:Fonction qui g�n�re une liste de valeurs de al�atoire a 100%
	//Arguments: Valeur de du nombre d'�l�ment K
	//			 Valeur de la puissance R
	//Retourne: une liste de valeur al�atoire non manipul�.
	public static List<Integer> GenerateRandomSearchArray(int aN, int aR)
 	{
 		//Declaration des variables	 		
 		List<Integer> T_R_array = new ArrayList<Integer>();
 		Random random = new Random();
	    int END = 5*(int)Math.pow(10, aR);
	    for(int i=0;i<aN;i++)
	    {		    	
	    	int temp_value = random.nextInt(END);		    		
		    T_R_array.add(temp_value);
	    	
	    }	

 		return T_R_array;
 		
 	}
	//Nom:GenerateRandomArray
	//Description:Fonction pricipale de g�n�ration de valeurs al�atoires, c'est cette 
	//			  fonction qui instaure le degr� de d�sordre.
	//Arguments: Valeur du nombre d'�l�ment N
	//			 Valeur de la puissance R
	//			 Valeur du degr� de d�sordre D
	//Retourne: une liste de valeur al�atoire non tri� mais avec une valeur de d�sordre.
	public static List<Integer> GenerateRandomArray(int aN, int aR, int aD)
	 	{
	 		 List<Integer> ViewList = new ArrayList<Integer>();
	 		//Declaration des variables	 		
	 		List<Integer> T_R_array = new ArrayList<Integer>();
	 		Random random = new Random();
		    int END = 8*(int)Math.pow(10, aR);
		    Stats.Search_Results.R = END;
		    int M = aN/2;
		    List<Integer> Croisant_array = new ArrayList<Integer>();
		    for(int i=0;i<M;i++)
		    {
		    	Croisant_array.add(i);
		    }
		    int[] array =new int[aN];
		    int[] array_sorted =new int[aN];
		    for(int i=0;i<aN;i++)
		    {		    	
		    	int temp_value = random.nextInt(END);		    		
			    T_R_array.add(temp_value);
		    	
		    }	
		    
		    for(int i=0;i<aN;i++)
		    {		 	
		    	 array[i] = T_R_array.get(i);
		    }
		    
		    array_sorted = TriPigeonnier.pigeonhole_sort(array);
		   
		    int m=M;
		    for(int i=1; i<(M*aD)/100;i++)
		    {
		    	int  K = random.nextInt(END)%m;
		    	int L= Croisant_array.get(K);
		    	Croisant_array.set(K, Croisant_array.get(m-1));	
		    	m=m-1;
		    	int tempval = array_sorted[M-L-1];
		    	array_sorted[M-L-1]=array_sorted[M+L];
		    	array_sorted[M+L]=tempval;	    	
		    }
		    
		    for(int i=0;i<aN;i++)
		    {	
		    	
		    	ViewList.add(array_sorted[i]);
		    }	
	 		return ViewList;
	 		
	 	}
	
	//Nom:log
	//Description:Fonction qui sert a l'affichage dans la console.
	//Arguments: Message a imprimer dans la console 
	//Retourne: void
	//ref:http://www.javapractices.com/topic/TopicAction.do?Id=62
	public static void log(String aMessage){
		    System.out.print(aMessage);
		  }
	
}
