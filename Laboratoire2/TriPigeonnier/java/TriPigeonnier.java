package TriPigeonnier.java;
import java.util.*;
//import Stats.java.Stats;

public class TriPigeonnier
{
	public static List<Integer> Sort(List<Integer> aArraylist)
	{
		
		//Stats.barometre(0);
		List<Integer> Returned_List = new ArrayList<Integer>();
		int[] array = new int[aArraylist.size()];	
		for(int i = 0; i<aArraylist.size(); i++)
		{
			//Stats.barometre(1);
			array[i] = aArraylist.get(i);
		}
		array = pigeonhole_sort(array);	
		for(int i = 0; i<aArraylist.size(); i++)
		{
			Returned_List.add(array[i]);
		}
			return  Returned_List;
	}
	//https://en.wikibooks.org/wiki/Algorithm_Implementation/Sorting/Pigeonhole_sort
	public static int[] pigeonhole_sort(int[] a)
	{
	    // size of range of values in the list (ie, number of pigeonholes we need)
	    int min = a[0], max = a[0];
	    for (int x : a) {
	    	//Stats.barometre(2);
	        min = Math.min(x, min);
	        max = Math.max(x, max);
	    }
	    //Stats.barometre(3);
	    final int size = max - min + 1;

	    // our array of pigeonholes
	    int[] holes = new int[size];  

	    // Populate the pigeonholes.
	    for (int x : a)
	    {
	    	//Stats.barometre(4);
	        holes[x - min]++;
	    }
	    // Put the elements back into the array in order.
	    int i = 0;
	    for (int count = 0; count < size; count++)
	    {
	    	//Stats.barometre(5);
	    	 while (holes[count]-- > 0)
		            a[i++] = count + min;
	    }	          	
	    return a;
	}
}