package Main;
import Generator.Generator;
import File_mgmt.manage_table;
import ParcoursDijkstra.Dijkstra;
import File_mgmt.*;
import ParcoursDijkstra.*;
import Noeud.*;

import java.util.*;
public class Main
{
	//Nom:GetResults
	//Description:Fonction principal du logiciel
	//Arguments: Choix du mode (1 ou 2)
	//			 Choix de l'algorithm(1 a 4)
	//			 Choix du problem(1 a 3)
	//			 Parametres (N, LienMin, LienMax, Poidmin, PoidMax)
	//Retourne:Null
	public static void GetResults(int Mode, int Algorithm, int Problem, int[] Params, String Source, String Destination, String Filename)
	{
		System.out.flush(); 
		//ArrayList<ArrayList<Integer>> Graph = new ArrayList<ArrayList<Integer>>();
		//
		
		//Test pour DANNY**********************************************
		

		//System.out.print("Test de matrice \n\n");
		//String FileName = "C:/Users/Andrew Surface/Desktop/lab3.txt";
		//manage_table.printMatrix(manage_table.Matrix(FileName), 19);
		//List<Node> NewList = new ArrayList<Node>();
		//NewList = listeNode.crationListeNoeuds(manage_table.Matrix(FileName), 19);

		//Dijkstra.DijkstraOptimize(NewList,Source, Destination ,Params[0]);
		//FIN Test pour DANNY**********************************************

		System.out.print("Test de matrice \n\n");
//		String FileName = "C:/Users/Danny/OneDrive/University/ELE/ELE440/LAB3/graphe_exemple.txt";
//		manage_table.printMatrix(manage_table.Matrix(FileName), 19);
//		listeNode alisteNode = listeNode.crationListeNoeuds(manage_table.Matrix(FileName), 19);
//
//		//confirmation du fonctionnement  du system
//		int ranger = 1; // Les lignes commence a 0
//		int numDevoisin = 1;// La liste des voisin commence aussi a 0
//		System.out.print("Test Test Test\n\n");
//		System.out.print(listeNode.listeDeNoeudAuPoid(alisteNode, ranger, numDevoisin));
//		System.out.print(" \n\nTest Test Test \n\n");
		//FIN Test pour DANNY**********************************************
		
		
		
		
		//String FileName1 = "C:/Users/Andrew Surface/Desktop/lab3.txt";
//		manage_table.printMatrix(manage_table.Matrix(FileName), 19);
		//List<Node> NewList = new ArrayList<Node>();
		//NewList = listeNode.crationListeNoeuds(manage_table.Matrix(FileName1), 19);

//		Dijkstra.DijkstraOptimize(NewList,Source, Destination ,Params[0]);
		
	
		
		if(Mode == 1)
		{
			int[][] RandomGraph = new int[Params[0]][Params[0]];
			RandomGraph = Generator.GenerateGraph(Params);
			Log("\n");
			for(int i = 0;i<Params[0];i++)
			{
				for(int j = 0; j<Params[0]; j++)
				{
					if(RandomGraph[i][j] != -1)
					{
						Log(RandomGraph[i][j]+"\t");
					}
					else
					{
						Log(RandomGraph[i][j]+"\t");
					}
					
				}
				Log("\n");
			}

				switch (Algorithm)
				{
				case 1:
					Problem = 3;
					break;
				case 2 :
					//Dijkstra.dijkstra(RandomGraph, 0, Params[0]);
					break;
				case 3:
					Problem = 3;
					break;
				case 4 :
					long tStart = System.currentTimeMillis();
					int source = Integer.parseInt(Source); 
					int destination = Integer.parseInt(Destination); 
					Dijkstra.DijkstraOptimize(RandomGraph,source,Params[0], destination);
					
					long tEnd = System.currentTimeMillis();
					long Tempcalcule = tEnd-tStart;
//					 Log("Temp de calcule est de :" +tStart + "\n");
//					 Log("Temp de calcule est de :" +tEnd + "\n");
					 Log("Temp de calcule est de :" +Tempcalcule + "\n");
					break;
				case 5:
					Problem = 5;
					break;
				}
			
			
		}
		//PArtie qui genere a partir du fichier
		else
		{
			switch (Algorithm)
			{
			case 1:
				Problem = 3;
				break;
			case 2 :
				//Dijkstra.dijkstra(RandomGraph, 0, Params[0]);
				break;
			case 3:
				Problem = 3;
				break;
			case 4 :
				long tStart = System.currentTimeMillis();
				int [][] Matrix = File_mgmt.manage_table.Matrix(Filename);
				manage_table.printMatrix(Matrix, Params[0]);
				int source = Integer.parseInt(Source); 
				int destination = Integer.parseInt(Destination); 
				Dijkstra.DijkstraOptimize(Matrix,source,Params[0], destination);
				long tEnd = System.currentTimeMillis();
				long Tempcalcule = tEnd-tStart;
				 Log("Temp de calcule est de :" +Tempcalcule + "\n");
				 //Log("Temp de calcule est de :" +tEnd + "\n");
				break;
			case 5:
				Problem = 5;
				break;
			}
		}
	}
	
	public static void Log(String Arguments)
	{
			System.out.print(Arguments);
	}
}