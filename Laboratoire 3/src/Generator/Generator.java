package Generator;
import java.util.*;

import Main.Main;
public class Generator
{
	//Nom:GenerateGraph
	//Description:
	//Arguments: Parametres (N, LienMin, LienMax, PoidMin, PoidMax)
	//Retourne:Null
	public static int[][] GenerateGraph(int[] Params)
			{
				for(int i= 0; i<5;i++)
				{
					Main.Log(Params[i]+ "	");
				}
				Random rand = new Random();			
				int[][] GeneratedGraph = new int[Params[0]][Params[0]];
				int[] NumerosLien = new int[Params[0]];
				int[] TpLien = new int[Params[0]];
				int K = 0, P=0;
				int v =0, a=0;
				
					for(int i = 0;i<Params[0];i++)
					{
						for(int j = 0; j<Params[0]; j++)
						{
							if(i == j)
								GeneratedGraph[i][j] = 0;
							else
								GeneratedGraph[i][j] = -1;
							if(Params[2] != Params[1])
							{
								NumerosLien[j] = rand.nextInt(Params[2])+(Params[1]);
							}
							else
							{
								NumerosLien[j] = rand.nextInt()%(Params[2]);
							}							
						}
					}
					Main.Log("\n");
					for(int k=0; k< Params[1];k++)
					{
						for(int n=0;n<Params[0];n++)
						{
							if(NumerosLien[n] > 0)
							{
								K=0;
							
								for(int p=0;p<(Params[0]);p++)
											{
											if(GeneratedGraph[n][p]<0 && NumerosLien[p]>0)
											{
												TpLien[K] = p;
												K++;
											}
										}
										if(K>0)
										{
											v = rand.nextInt(K);
											a = TpLien[v];
											if(Params[4] != Params[3])
											{
												P = Math.abs(rand.nextInt()%(Params[4]-Params[3])) ;	
												if(P == 0)
												{
													P = 1;
												}
											}
											else
											{
												if(Params[4] != 1 || Params[4] == Params[3])
												{
													P = 1;
												}
												else
												{
													P = rand.nextInt()%(Params[4]);
													
												}
																						
											}
											GeneratedGraph[n][a] = P;
											GeneratedGraph[a][n] = P;
											NumerosLien[n] = NumerosLien[n] -1;
											NumerosLien[a] = NumerosLien[a] -1;	
										}
									}
						}	
					}
					

					
				return GeneratedGraph;
			}
}