package Noeud;

import java.util.ArrayList;

import File_mgmt.manage_table;

import java.lang.Integer;


public class listeNode {
	
	private static ArrayList<Node> listeDeNoeud;
	private static int nbNode; //Nombre totale de noeud dans la liste
	
	
	public listeNode(ArrayList<Node> listeDeNoeud,int nbNode){
		this.listeDeNoeud = listeDeNoeud;
		this.nbNode = nbNode;
	}
	
	
	public ArrayList<Node> arrayNode(){
		return listeDeNoeud; //Nombre totale de noeud dans la liste
	}
	
	public static int listeDeNoeudAuPoid(listeNode listeNode, int ligne , int numVoisin){
		Node noeud = listeNode.arrayNode().get(ligne);
		int poid = noeud.getPoidsLiens().get(numVoisin);
		return poid; //Nombre totale de noeud dans la liste
	}
	
	public int nbNoeud(){
		
		return nbNode; //Nombre totale de noeud dans la liste
	}	
	
	public static listeNode crationListeNoeuds(int [][] matrix, int size){
		
		ArrayList<Node> listeDeNoeud = new ArrayList<Node>();
		listeNode Listecourante = new listeNode(listeDeNoeud,size);
		
		for(int i=0; i<size; i++){
			//Cration d'un noeud par ligne de la matrice (Rang�)
			Node noeudCourant = nouveauNoeud(i);
			int change=0;
			
			for(int j=0; j<size; j++){
				// Ne prend pas en compte la diagonal et les valeur de -1 repr�sente infini
				if (j != i ) { 		
					if (matrix[i][j]>0){
						int teste2 = matrix[i][j];
						noeudCourant.addvoisin(matrix[i][j], j);
					}
				}
			}
		listeDeNoeud.add(i, noeudCourant);
        }
		
		return Listecourante;
	}
	
	private static Node nouveauNoeud(int range){
		String name = Integer.toString(range);
		ArrayList<Integer> arrayVide1 = new ArrayList<Integer>();
		ArrayList<Integer> arrayVide2 = new ArrayList<Integer>();
		Node nouveauNoeud = new Node (name ,false, 0,arrayVide1,arrayVide2);
		return nouveauNoeud;
	}
	
	
}
