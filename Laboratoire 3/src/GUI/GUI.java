package GUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;

import Main.Main;
import net.miginfocom.swing.MigLayout;
import javax.swing.BoxLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ItemEvent;
import java.awt.Label;
import javax.swing.JButton;

public class GUI {

	private JFrame frame;
	private JTextField NtxtField;
	private JTextField txtLienMinimum;
	private JTextField txtLienMaximum;
	private JTextField txtPoidMaximum;
	private JTextField txtPoidMinimum;
	private JTextField txtFileName;
	private Label label_1;
	private JTextField txtSource;
	private JTextField txtDestination;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setFont(new Font("Tahoma", Font.PLAIN, 18));
		frame.setBounds(100, 100, 589, 410);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JLabel lblNewLabel = new JLabel("Nombre de noeud Selectionn\u00E9:");
		lblNewLabel.setBounds(7, 66, 236, 20);
		
		NtxtField = new JTextField();
		NtxtField.setBounds(238, 63, 146, 26);
		NtxtField.setColumns(10);
		
		txtLienMinimum = new JTextField();
		txtLienMinimum.setBounds(92, 119, 146, 26);
		txtLienMinimum.setColumns(10);
		
		txtLienMaximum = new JTextField();
		txtLienMaximum.setBounds(411, 119, 76, 26);
		txtLienMaximum.setColumns(10);
		
		txtPoidMaximum = new JTextField();
		txtPoidMaximum.setBounds(411, 175, 76, 26);
		txtPoidMaximum.setColumns(10);
		
		txtPoidMinimum = new JTextField();
		txtPoidMinimum.setBounds(92, 175, 146, 26);
		txtPoidMinimum.setColumns(10);
		
		JLabel lblNombreDeLien = new JLabel("Nombre de Liens");
		lblNombreDeLien.setBounds(7, 93, 266, 22);
		lblNombreDeLien.setFont(new Font("Tahoma", Font.BOLD, 18));
		
		JLabel lblPoidDeChaque = new JLabel("Poid de chaque Lien(Laisser vide si non-Pond\u00E9r\u00E9)");
		lblPoidDeChaque.setBounds(7, 149, 445, 22);
		lblPoidDeChaque.setFont(new Font("Tahoma", Font.BOLD, 18));
		
		JLabel lblNewLabel_1 = new JLabel("Maximum:");
		lblNewLabel_1.setBounds(323, 122, 75, 20);
		
		JLabel label = new JLabel("Maximum:");
		label.setBounds(252, 178, 75, 20);
		
		JLabel lblMinimum = new JLabel("Minimum:\r\n");
		lblMinimum.setBounds(5, 178, 72, 20);
		
		JLabel label_2 = new JLabel("Minimum:\r\n");
		label_2.setBounds(7, 122, 72, 20);
		
		JComboBox ModecomboBox = new JComboBox();		
		ModecomboBox.setBounds(7, 33, 266, 26);
		ModecomboBox.setModel(new DefaultComboBoxModel(new String[] {"Generer", "Fichier"}));
		
		JLabel lblChoixDuType = new JLabel("Choix du Type de Donn\u00E9:");
		lblChoixDuType.setBounds(7, 7, 266, 22);
		lblChoixDuType.setFont(new Font("Tahoma", Font.BOLD, 18));
		
		txtFileName = new JTextField();
		txtFileName.setText("C:/Users/Andrew Surface/Desktop/lab3.txt");
		txtFileName.setBounds(411, 33, 146, 26);
		txtFileName.setColumns(10);
		
		
		JLabel lblNomDuFichier = new JLabel("Nom du fichier");
		lblNomDuFichier.setBounds(295, 36, 192, 20);
		frame.getContentPane().setLayout(null);
		
		frame.getContentPane().add(lblChoixDuType);
		frame.getContentPane().add(ModecomboBox);
		frame.getContentPane().add(lblNomDuFichier);
		frame.getContentPane().add(txtFileName);
		frame.getContentPane().add(lblNewLabel);
		frame.getContentPane().add(NtxtField);
		frame.getContentPane().add(lblNombreDeLien);
		frame.getContentPane().add(lblMinimum);
		frame.getContentPane().add(txtPoidMinimum);
		frame.getContentPane().add(label);
		frame.getContentPane().add(txtPoidMaximum);
		frame.getContentPane().add(label_2);
		frame.getContentPane().add(txtLienMinimum);
		frame.getContentPane().add(lblNewLabel_1);
		frame.getContentPane().add(txtLienMaximum);
		frame.getContentPane().add(lblPoidDeChaque);
		
		label_1 = new Label("Choix de l'algorithm :\r\n");
		label_1.setBounds(7, 238, 177, 27);
		frame.getContentPane().add(label_1);
		
		JComboBox AlgorithmcomboBox = new JComboBox();
		AlgorithmcomboBox.setModel(new DefaultComboBoxModel(new String[] {"Exploration en Profondeur", "Exploration en Largeur", "Exploration en profondeur de type glouton", "Djikstra", "Floyd Warshall"}));
		AlgorithmcomboBox.setBounds(190, 238, 305, 26);
		frame.getContentPane().add(AlgorithmcomboBox);
		
		Label label_3 = new Label("Choix du probleme:\r\n");
		label_3.setBounds(7, 206, 177, 27);
		frame.getContentPane().add(label_3);
		
		JComboBox ProblemcomboBox = new JComboBox();
		ProblemcomboBox.setModel(new DefaultComboBoxModel(new String[] {"Tour de Londre", "Labyrinthe", "Carte Routiere"}));
		ProblemcomboBox.setBounds(190, 206, 305, 26);
		frame.getContentPane().add(ProblemcomboBox);
		
		JButton btnGenerate = new JButton("Generate Results");
		btnGenerate.setBounds(7, 307, 550, 29);
		frame.getContentPane().add(btnGenerate);
		
		txtSource = new JTextField();
		txtSource.setColumns(10);
		txtSource.setBounds(97, 271, 146, 26);
		frame.getContentPane().add(txtSource);
		
		txtDestination = new JTextField();
		txtDestination.setColumns(10);
		txtDestination.setBounds(394, 271, 146, 26);
		frame.getContentPane().add(txtDestination);
		
		JLabel lblSource = new JLabel("Source:");
		lblSource.setBounds(37, 271, 72, 20);
		frame.getContentPane().add(lblSource);
		
		JLabel lblDestination = new JLabel("Destination:");
		lblDestination.setBounds(293, 274, 91, 20);
		frame.getContentPane().add(lblDestination);
		btnGenerate.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				int Mode =1 , Problem =1, Algorithme = 1;
				int N = 1;
				int LienMax = 2, LienMin =2;
				int PoidMin =2 , PoidMax =1;
				int Nbr_Search = 1;
				String Source = "0", Destination = "2";
				
				
			switch (ModecomboBox.getSelectedItem().toString())
			{
			case "Generer":
				Mode = 1;
				break;	
			case "Fichier":
				Mode = 2;
				break;
			}
			
			switch (ProblemcomboBox.getSelectedItem().toString())
			{
			case "Tour de Londre":
				Problem = 1;
				break;	
			case "Labyrinthe":
				Problem = 2;
				break;
			case "Carte Routiere":
				Problem = 3;
				break;
			}
			
			switch (AlgorithmcomboBox.getSelectedItem().toString())
			{
			case "Exploration en Profondeur":
				Algorithme = 1;
				break;	
			case "Exploration en Largeur":
				Algorithme = 2;
				break;
			case "Exploration en profondeur de type glouton":
				Algorithme = 3;
				break;
			case "Djikstra":
				Algorithme = 4;
				break;
			case "Floyd Warshall":
				Algorithme = 5;
				break;
			}
			
			try{
				if(Integer.parseInt(NtxtField.getText()) >=1)
					N = Integer.parseInt(NtxtField.getText());				
			}
			catch(Exception e)
			{
				N=1;				
			}	
			
			try{
				if(Integer.parseInt(txtLienMaximum.getText()) >=2)
				LienMax = Integer.parseInt(txtLienMaximum.getText());		
			}
			catch(Exception e)
			{
				LienMax = 2;
			}
			try{
				if(Integer.parseInt(txtLienMinimum.getText()) >=2)
				LienMin = Integer.parseInt(txtLienMinimum.getText());		
			}
			catch(Exception e)
			{
				LienMin = 2;
			}
			
			try{
				if(Integer.parseInt(txtPoidMinimum.getText()) >=2)
				PoidMin = Integer.parseInt(txtPoidMinimum.getText());		
			}
			catch(Exception e)
			{
				PoidMin = 1;
			}
			
			try{
				if(Integer.parseInt(txtPoidMaximum.getText()) >=2)
				PoidMin = Integer.parseInt(txtPoidMaximum.getText());		
			}
			catch(Exception e)
			{
				PoidMin = 1;
			}
			try{
				Source = txtSource.getText();		
			}
			catch(Exception e)
			{
				Source = "0";
			}
			try{
				Destination = txtDestination.getText();		
			}
			catch(Exception e)
			{
				Destination = "1";
			}
			String filename = txtFileName.getText();
			int[] Params  = new int[]{N,LienMin, LienMax, PoidMin, PoidMax};
			Main.GetResults(Mode, Algorithme, Problem, Params, Source, Destination, filename);
			}
			
		});
	}
}
