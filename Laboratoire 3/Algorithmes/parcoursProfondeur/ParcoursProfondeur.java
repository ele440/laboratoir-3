package parcoursProfondeur;

import java.util.*;

public class ParcoursProfondeur {

	class Node {
		int Nom;
		boolean Visite;
		int NVoisins;
		List<Node> PoidsLiens;
		List<Node> IndexVoisins;
	}
	
	// au commencement il faut que la fonction noeud mettre .visite � 0, .PoidsLiens � -1, .IndexVoisins � -1
		public static Boolean Profondeur(List<Node> Ad, List<Integer> Chemin, int Source, int Destination, double Temps) {
		
			long tStart = System.currentTimeMillis();
			List<Node> autresChemin = new ArrayList<Node>(); // contient les enfants non visit�s
			List<Node> A = new ArrayList<Node>();
			int numeroVoisin, nomVoisin, noeudCourant, prochainNoeud;
			
			A.get(Source).Visite = true;
			Chemin.add(Source);
			noeudCourant = Source;
			
			while(noeudCourant != Destination) {
				
				if(A.get(noeudCourant).NVoisins > 1) {
					// boucle tant que le voisin est visit�
					do  {
							numeroVoisin = ChoisirVoisin(noeudCourant, A.get(noeudCourant).NVoisins);
							nomVoisin = A.get(noeudCourant).IndexVoisins.get(numeroVoisin).Nom;
					} while(A.get(numeroVoisin).Visite == true);
				}
				else {
					numeroVoisin = 0;
					nomVoisin = A.get(noeudCourant).IndexVoisins.get(numeroVoisin).Nom;
				}
				
				Chemin.add(nomVoisin);
				A.get(nomVoisin).Visite = true;
				prochainNoeud = A.get(nomVoisin).Nom;
				
				if(!A.get(prochainNoeud).IndexVoisins.isEmpty()){ // s'assure que le nouveau noeud a des voisins
					// pas s�r du fonctionnement de ces 2 lignes
					A.get(noeudCourant).IndexVoisins.remove(numeroVoisin); // enleve l'enfant dans la liste du parent, car n'y retournera plus
					A.get(noeudCourant).NVoisins -= 1; // son nb de voisins diminu
					autresChemin.addAll(A.get(noeudCourant).IndexVoisins); // copie les index des enfants dans une liste, pour revenir direct l� en cas de cul-de-sac
					
					noeudCourant = prochainNoeud ;
				}
				else {
					noeudCourant = autresChemin.get(autresChemin.size()-1).Nom; // prend le dernier noeud non choisi
					autresChemin.remove(autresChemin.size()-1); // enleve ce noeud de la liste des non visit�s
				}
				
				Chemin.add(noeudCourant);
			}
			
			long tEnd = System.currentTimeMillis();
			long tDelta = tEnd - tStart;
			Temps = tDelta / 1000.0;
			return true;
		}
		
		// nb al�atoire pour choisir le voisin
		public static int ChoisirVoisin(int noeudCourant, int nbVoisins) {
			return (int) Math.round(Math.random()*(nbVoisins - 1)); 
		}
		
	
	
	
	/*public static Boolean Profondeur((List<Node> A, List<Node> Chemin, int Source, int Destination, double Temps) {
		
		Boolean Trouve;
		int k, v;
		
		if (A.get(Source).visite) {
			return false;
		}
		else {
			Trouve = false;
		}
		
		while(k<=A.get(Source).NVoisins && !Trouve) {
			v = A.get(Source).Poidsliens[k];
			if(A.get(v).nom == Destination) {
				Trouve = true;
				Chemin[p+1] = A.get(v).nom;
			}
			else {
				Trouve = Profondeur(A, v, Destination, Chemin, p+1);
			}
			
			if(!Trouve) {
				k = k + 1;
			}
		}
		
		if(Trouve) {
			Chemin[p] = A.get(Source).nom;
		}
		
		A.get(Source).visite = Trouve;
	}	*/
}
