package RechercheSequentiel.java;
import java.util.*;


import Generation.java.generation;
import Stats.java.*;
public class RechercheSequentiel
{
	private static int found = 0;
	//Nom:Search
	//Description:Fonction pricipale qui cherche toute les valeur d'un tableau dans une autre tableau.
	//Arguments: Tableau contenant tout les �l�ments
	//			 Tableau contenant tout les �l�ments � recherch�
	//Retourne: une liste null
	public static List<Integer> Search(List<Integer> aTable, List<Integer> aSearchList)
	{
		List<Integer> SearchResult = new ArrayList<Integer>();
		List<Integer> SearchArrayList = new ArrayList<Integer>();
		SearchArrayList = aSearchList;
		Stats.End_Time();
		Stats.Calcule_TP();
		Stats.Start_Time();
		for(int j=1;j<SearchArrayList.size();j++)
		{			
			Stats.Iteration_Counter = 0;
			
			if(linearSearch(aTable ,SearchArrayList.get(j)) == false)
			{
				
				if(Stats.Iteration_Counter != 0 && found == 1)
				{					
					Stats.Calcule_TR();
					Stats.Search_Results.Iteration.add(Stats.Iteration_Counter);
					Stats.Search_Results.Found_values++;
				}
				else
				{
					Stats.Search_Results.Iteration.add(-1);
				}
					
			}	
			
		}
		Stats.End_Time();
		return SearchResult;
	}
	
	//linearSearch
	//Description:Fonction fait la comparaison et qui incr�mente la position du compteur.
	//Arguments: Le tableau dans laquelle nous cherchons la valeur
	//			 Valeur rechercher
	//Retourne: faux et la variable found incremente si la variable est trouv�e.
	//http://www.cs.utoronto.ca/~reid/search/lincode.html
	 public static boolean linearSearch(List<Integer>data, int key) 
	    {
		
	        int index = 0;
	        found = 0;
	        while(index < data.size() && found ==0) {
	        	 
	        	Stats.Iteration_Counter = Stats.Iteration_Counter + 1;
	        	
	             if(data.get(index) == key) {
	            	 
	            	 found = 1;
	             }
	              index++;
	              
	         }
	         return false;
	   }
}
 