package TriParTas.java;

import java.util.*;

import Stats.java.Stats; 


public class TriParTas 
{    
    public static List<Integer> Sort(List<Integer> aArraylist)
    {
    	Stats.barometre(0);
    	List<Integer> Returned_List = new ArrayList<Integer>();
    	int[] array = new int[aArraylist.size()];
    	
    	for(int i=0;i<aArraylist.size();i++)
    	{
    		Stats.barometre(1);
    		array[i] = aArraylist.get(i);
    	}
    	array = Heap_sort(array);   	
		for(int i = 0; i<aArraylist.size(); i++)
		{
			Returned_List.add(array[i]);
		}
    	return Returned_List;
    }
    //Ref:http://eddmann.com/posts/implementing-heapsort-in-java-and-c/
    //debut
    public static void log(String aMessage){
	    System.out.println(aMessage);
	  }
    private static int total;

    private static void swap(int[] arr, int a, int b)
    {
    	Stats.barometre(2);
        int tmp = arr[a];
        arr[a] = arr[b];
        arr[b] = tmp;
    }

    private static void heapify(int[] arr, int i)
    {
        int lft = i * 2;
        int rgt = lft + 1;
        int grt = i;
        Stats.barometre(3);
        if (lft <= total && ((arr[lft]-(arr[grt])) > 0))
        	{
        	Stats.barometre(4);
        	grt = lft;
        	}
        
        if (rgt <= total && ((arr[rgt]-(arr[grt])) > 0))
        	{
        	Stats.barometre(5);
        	grt = rgt;
        	}
        if (grt != i) {
        	Stats.barometre(6);
            swap(arr, i, grt);
            heapify(arr, grt);
        }
    }

    public static int[] Heap_sort(int[] arr)
    {
        total = arr.length - 1;
        Stats.barometre(7);
        for (int i = total / 2; i >= 0; i--)
        {
        	Stats.barometre(8);
            heapify(arr, i);
        }
        for (int i = total; i > 0; i--) {
        	Stats.barometre(9);
            swap(arr, 0, i);
            total--;
            heapify(arr, 0);
        }
        return arr;
    }
    //fin
}