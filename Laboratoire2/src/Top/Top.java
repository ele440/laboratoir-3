package Top;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.io.*;
import java.math.*;

import Controleur.java.*;
import Generation.java.*;
import Stats.java.*;


public class Top {

	
	public static void GetResults(int choix_Mode, int choix_Tri, int choix_Recherche, int N, int R, int D, int Nbr_Searched) {
		Scanner commande = new Scanner(System.in);
		List<Integer> Tableau1 = new ArrayList<Integer>();
		List<Integer> Tableau2 = new ArrayList<Integer>();
		List<Integer> SearchValues = new ArrayList<Integer>();
		List<Integer> TempSearchValues = new ArrayList<Integer>();
		
		int [] NRD = new int[3];
		String Algo = null;
		//L'utilisateur � choix de g�n�rer des donn�es al�atoires
			if(choix_Mode==1) {
				//D�but du calcule du temps de calcule de pour le temp de pr�paration
				Stats.Start_Time();
				Stats.Search_Results.K = Nbr_Searched;
				//Fonction retourne le tableau g�n�r� al�atoire selon les param�tres N, R , D
				Tableau1 = Controleur.SortingFeature(choix_Mode, N, R, D);
				//Fonction qui retourne un tableau de donn�es qui sera a cherch�.
				SearchValues = generation.GenerateRandomSearchArray(Nbr_Searched, R);
				//SearchValues.add(Tableau1.get(500));
				NRD = Controleur.Parametres();
				if(choix_Tri != 7)
				{
					//Proc�dure qui trie le tableau g�n�r� selon l'algorithme de tri selectionn�.
					Tableau2 = Controleur.Algo(choix_Tri, Tableau1);
					//Proc�dure qui Recherche les valeurs du 3em arguments dans le tableau dans le 2em argument
					//selon l'algorithme de recherche voulue.
					Controleur.SearchAlgo(choix_Recherche , Tableau2, SearchValues);
					Algo = Controleur.NomAlgo(choix_Tri);
				}
				else
				{
					//Proc�dure de Recherche avec un tableau non-tri�
					Tableau2 = Tableau1;
					Controleur.SearchAlgo(choix_Recherche , Tableau2, SearchValues);
					Algo = Controleur.NomAlgo(choix_Tri);
				}

			}
			//L'utilisateur a choisie la m�thode du fichier existant.
			if(choix_Mode==2) {
				Stats.Start_Time();
				Tableau1 = Controleur.SortingFeature(choix_Mode, N, R, D);				
				TempSearchValues = Controleur.SearchFeature();
				Stats.Search_Results.K = TempSearchValues.get(0);
				for(int i = 1; i<TempSearchValues.size();i++)
				{
					
					SearchValues.add(TempSearchValues.get(i));
				}
				NRD = Controleur.Parametres();
				if(choix_Tri != 7)
				{
					//Tableau2 = Controleur.Algo(choix_Tri, Tableau1);
					Controleur.SearchAlgo(choix_Recherche , Tableau1, SearchValues);
					Algo = Controleur.NomAlgo(choix_Tri);
				}
				else
				{
					Tableau2 = Tableau1;
					Algo = Controleur.NomAlgo(choix_Tri);
				}
				
			}
			
		generation.log("Au revoir");
		commande.close();
	}	
}

