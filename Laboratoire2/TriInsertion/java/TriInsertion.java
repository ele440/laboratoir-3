package TriInsertion.java;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import Generation.java.*;
import Stats.java.Stats;

// source : http://www.java2novice.com/java-sorting-algorithms/insertion-sort/
public class TriInsertion {
	 
	     	// Ce tri re�oit un Arraylist, est-ce correct ou il faut un tableau ?
	    public static List<Integer> insertionSort(List<Integer> T1){
	    	Stats.barometre(0);
	    	List<Integer> T2 = new ArrayList<Integer>(T1);
	        int temp;
	        for (int i = 1; i < T2.size(); i++) {
	        	Stats.barometre(1);
	            for(int j = i ; j > 0 ; j--){
	            	Stats.barometre(2);
	                if(T2.get(j) < T2.get(j-1)) {
	                	Stats.barometre(2);
	                    temp = T2.get(j);
	                    T2.set(j, T2.get(j-1));
	                    T2.set(j-1, temp);
	                }
	            }
	        }
	        return T2;
	    }
	}
