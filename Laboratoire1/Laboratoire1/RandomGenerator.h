#pragma once
#ifndef RANDOMGENERATOR_H
#define  RANDOMGENERATOR_H 

#include <iostream>
#include <fstream>
#include <time.h>
#include <stdlib.h>
#include <math.h>

void Create_File();
int Generate_Random_Number(int R);
void Create_Random_Table(int N, int R, int D);
int Create_Header_File(int N, int R, int D);

#endif // !RANDOM_GENERATOR_H

