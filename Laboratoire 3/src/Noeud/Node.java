package Noeud;

import java.util.ArrayList;


		 //http://www.vogella.com/tutorials/JavaAlgorithmsDijkstra/article.html#shortestpath_graph
	
public class Node {

        final private String Nom;
        private Boolean Visite;
        private int NVoisins;
        private ArrayList<Integer> PoidsLiens;
        private ArrayList<Integer> IndexVoisins;
        
        
        public Node(String Nom, Boolean Visite,int NVoisins,ArrayList<Integer> PoidsLiens,ArrayList<Integer> IndexVoisins) {
                this.Nom = Nom;
                this.Visite = Visite;
                this.NVoisins = NVoisins;
                this.PoidsLiens = PoidsLiens;
                this.IndexVoisins = IndexVoisins;
        }
        
        // Obtenir l'information de l'object en question
        public Boolean getVisite() {
                return Visite;
        }

        public String getName() {
                return Nom;
        }
        
        public int getNVoisins() {
            return NVoisins;
	    }
	
	    public ArrayList<Integer> getPoidsLiens() {
            return PoidsLiens;
	    }
	    
	    public ArrayList<Integer> getIndexVoisins() {
	            return IndexVoisins;
	    }
	    
	    //Modification de l'object en question
        public Boolean addVisite() {
        	 return Visite = true;
	    }
        public Boolean resetVisite() {
            return Visite = false;
	    }
	    
	    private void addNVoisins() {
	        NVoisins ++;
	    }
	
	    private void addPoidsLiens(int newPoidsLien) {
	          PoidsLiens.add(NVoisins-1,newPoidsLien);
	    }
	    
		
	    private void addIndexVoisins(int newindexVoisin) {
	          IndexVoisins.add(NVoisins-1,newindexVoisin);
	    }

	    //Cration d'une noeud global (Publique)
	    public void addvoisin(int new2PoidsLien , int new2indexVoisin){
	    	addNVoisins();
	    	addPoidsLiens(new2PoidsLien);
	    	addIndexVoisins(new2indexVoisin);
	    	
	    }
}
