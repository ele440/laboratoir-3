package Controleur.java;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import Generation.java.generation;
import TriFusion.java.TriFusion;
import TriInsertion.java.TriInsertion;
import TriParTas.java.TriParTas;
//import TriFusion.java.TriFusion;
//import TriInsertion.java.TriInsertion;
//import TriParTas.java.TriParTas;
import TriPigeonnier.java.TriPigeonnier;
import TriRadixx.java.TriRadixx;
import TriRapide.java.TriRapide;
import RechercheSequentiel.java.*;
import Stats.java.Stats;
import RechercheBinaire.RechercheBinaire;
import AvlTree.java.*;

public class Controleur {
	
	private static int N, R, D;
	
	//@SuppressWarnings({ "resource", "unused" })
	//Fonction qui sert a trier les donn�es selon les
	public static List<Integer> SortingFeature(int choix, int aN, int aR, int aD) {
		FileInputStream FichierIN = null;
		List<Integer> Tableau1 = new ArrayList<Integer>();
		List<Integer> SeachParam = new ArrayList<Integer>();
		Scanner commande = new Scanner(System.in);
			
			switch(choix) {
				case 1: 
					
						Tableau1 = generation.Generator(aN, aR, aD);						
					break; 
				case 2: try {
						FichierIN = new FileInputStream(new File("T1.txt"));
						Scanner lectureFichierIN = new Scanner(FichierIN);
						N = lectureFichierIN.nextInt();
						
						R = lectureFichierIN.nextInt();
						D = lectureFichierIN.nextInt();
						while(lectureFichierIN.hasNextInt()) {
							Tableau1.add(lectureFichierIN.nextInt());
						}
						} catch (FileNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				SeachParam = SearchFeature();
						N = 1000/N;
						R = (int) Math.log10(R); // ne donne pas le bon nombre (peut-�tre probl�me de cast)
						R = R*5;
						Stats.Search_Results.R = R;
					break;		
			}
			commande.close();

			//SeachParam = SearchFeature();
			return Tableau1;
	}
	
	public static List<Integer> SearchFeature() {
		FileInputStream FichierIN = null;
		List<Integer> SeachParam = new ArrayList<Integer>();
		Scanner commande = new Scanner(System.in);
		
		try {
			FichierIN = new FileInputStream(new File("T2.txt"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Scanner lectureFichierIN = new Scanner(FichierIN);
		
		SeachParam.add(lectureFichierIN.nextInt()); // K
			while(lectureFichierIN.hasNextInt()) {
				SeachParam.add(lectureFichierIN.nextInt());
			}
		commande.close();
		return SeachParam;
		
	}
	
	public static int[] Parametres() {
		int[] NRD = new int[3];
		NRD[0] = N;
		NRD[1] = R;
		NRD[2] = D;
		
		return NRD;
	}
	
	public static void SearchAlgo(int aAlgo, List<Integer> aTableau, List<Integer> aSearchList)
	{
		switch(aAlgo) {
		case 2: 
			
			Stats.Search_Results.All_Data = aTableau;
			Stats.Search_Results.Searched_Values = aSearchList;
			RechercheSequentiel.Search(aTableau, aSearchList);						
			Stats.Get_Search_Results();
			Stats.SauverStats(Controleur.Parametres(),"RechercheSequentiel" , 1);
			Stats.Reset_Search_Results();
			break;
		case 1: 
			Stats.Search_Results.All_Data = aTableau;
			Stats.Search_Results.Searched_Values = aSearchList;
			RechercheBinaire.Search(aTableau, aSearchList);
			Stats.Get_Search_Results();
			Stats.SauverStats(Controleur.Parametres(),"RechercheBinaire" , 1);
			Stats.Reset_Search_Results();
				break; 
		case 3: //Tableau2 = TriRapide.Quicksort(Tableau1);
			break;
		case 4: 
			break;
		case 5: 
			break;
		case 6: //Tableau2 = TriRadixx.radixsort(Tableau1);
			Stats.Search_Results.All_Data = aTableau;
			Stats.Search_Results.Searched_Values = aSearchList;
			AVLTree.search(aTableau, aSearchList);
			Stats.Get_Search_Results();
			Stats.Reset_Search_Results();
			break;
		}
	}
	
	public static List<Integer> Algo(int choix, List<Integer> Tableau1) {
		List<Integer> Tableau2 = new ArrayList<Integer>();
						
		switch(choix) {
			case 1: Tableau2 = TriInsertion.insertionSort(Tableau1);
				break;
			case 2: Tableau2 = TriPigeonnier.Sort(Tableau1);
					break; 
			case 3: Tableau2 = TriRapide.Quicksort(Tableau1);
				break;
			case 4: Tableau2 = TriFusion.mergeSort(Tableau1);
				break;
			case 5: Tableau2 = TriParTas.Sort(Tableau1);
				break;
			case 6: Tableau2 = TriRadixx.radixsort(Tableau1);
				break;
		}
		
		return Tableau2;
	}
	
	public static String NomAlgo(int choix) {
		String NomAlgo = null;
		
		switch(choix) {
		case 1: NomAlgo = "Insertion";
			break;
		case 2: NomAlgo = "Pigeonnier";
			break; 
		case 3: NomAlgo = "Rapide";
			break;
		case 4: NomAlgo = "Fusion";
			break;
		case 5: NomAlgo = "Heap";
			break;
		case 6: NomAlgo = "DeBase";
			break;
	}
	return NomAlgo;
		
	}

}
