package File_mgmt;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.lang.*;

public class manage_table {

	@SuppressWarnings("resource")
	public static int[][] MatrixFromFile(String filename) throws Throwable {
		
		Scanner input = new Scanner (new File(filename));
		// pre-read in the number of rows/columns
		int rows = 0;
		int columns = 0;
		while(input.hasNextLine())
		{
		    ++rows;
		    Scanner colReader = new Scanner(input.nextLine());
		    while(colReader.hasNextInt())
		    {
		        ++columns;
		    }
		}
		int[][] a = new int[rows][columns];

		input.close();

		// read in the data
		input = new Scanner(new File(filename));
		for(int i = 0; i < rows; ++i)
		{
		    for(int j = 0; j < columns; ++j)
		    {
		        if(input.hasNextInt())
		        {
		            a[i][j] = input.nextInt();
		            System.out.print(a[i][j]);
		        }
		        
		    }
		}
		return a;
	}

	//http://stackoverflow.com/questions/35147544/how-to-read-a-txt-file-into-a-2d-array-java
	public static int[][] Matrix (String FileName) 
	{
		int size =MatrixSize (FileName);
        int matrix[][] = new int[size][size];
		Scanner in;
        try {
            Scanner s = new Scanner(new File(FileName));
            	
            	//Passe la premi�re ligne
            	s.hasNextLine();
            	s.nextInt();
            
            	for (int i = 0; i < matrix.length && s.hasNextLine(); i++) {
                       for (int col = 0; col < matrix.length && s.hasNextInt(); col++) {
                    	   matrix[i][col] = s.nextInt() ;

                        }
                       //s.nextLine(); // col values populated for this row, time to go to the next line
                }


        } catch (IOException i) {
            System.out.println("Problems..");

        }
		return matrix;
       
	}
	
	
	
	
	
	
	
	
	
	public static int MatrixSize (String FileName) 
	{
		
		int size = 0;
        try {
            @SuppressWarnings("resource")
			Scanner s = new Scanner(new File(FileName));

            	//Passe la premi�re ligne
            	s.hasNextInt(); 
            	size = s.nextInt() ;
  
                 //s.nextLine(); // col values populated for this row, time to go to the next line
               

        } catch (IOException i) {
            System.out.println("Problems..");

        }
		return size;
       
	}
	
	
	
	
	
	
	
	
	
	
	
	
	//http://stackoverflow.com/questions/5061912/printing-out-a-2-d-array-in-matrix-format
	public static void printMatrix(int matrix[][],int seize){
	
	for (int i = 0; i < seize; i++) {
		String Line = "";
	    for (int j = 0; j < seize; j++) {
	    	Line = Line +Integer.toString(matrix[i][j])+ " "; 
	    }
	    System.out.println(Line);
	    System.out.println();
	}
}
}
