#ifndef _TRI_INSERTION_
#define _TRI_INSERTION_

typedef struct t_noeud
{
	int valeur;
	struct t_noeud *suivant;

} t_noeud;


void creationListe(t_noeud **debut, t_noeud **fin, int valeur);
void deplacement(t_noeud *debut, t_noeud *fin, int N);
void voirListe(t_noeud* probe);
void Insert_value_File(int Value);

#endif