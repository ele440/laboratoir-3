package File_mgmt;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.*;

public class  manage_file {

	//source code: https://www.caveofprogramming.com/java/java-file-reading-and-writing-files-in-java.html#readtext
	public static void read_file(String fileName){


        // Reference d'une ligne � la fois
        String line = null;

        try {
            // Utilisation du code original du fichier
            FileReader fileReader = new FileReader(fileName);

            // toujours utiliser BufferedReader.
            BufferedReader bufferedReader =  new BufferedReader(fileReader);

            while((line = bufferedReader.readLine()) != null) {
                System.out.println(line);
            }   

            // Fermeture du fichier.
            bufferedReader.close();         
        }
        catch(FileNotFoundException ex) {
            System.out.println(
                "Unable to open file '" + 
                fileName + "'");                
        }
        catch(IOException ex) {
            System.out.println(
                "Error reading file '" 
                + fileName + "'");                  

        }
    }

}
