import java.awt.EventQueue;
import java.util.*;
import Controleur.java.*;
import Generation.java.*;
import Stats.java.*;
import Top.Top;
import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import java.awt.Component;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.GridLayout;
import java.awt.FlowLayout;
import javax.swing.JRadioButton;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.Toolkit;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JTextPane;

public class GUI {

	private JFrame frmLaboratoire;
	private JTextField txtN;
	private JTextField txtD;
	private JTextField txtR;
	private JTextField txtNbSearched;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frmLaboratoire.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmLaboratoire = new JFrame();
		frmLaboratoire.setResizable(false);
		frmLaboratoire.setIconImage(Toolkit.getDefaultToolkit().getImage(GUI.class.getResource("/com/sun/java/swing/plaf/windows/icons/Computer.gif")));
		frmLaboratoire.setTitle("Laboratoire 2");
		frmLaboratoire.setBackground(Color.BLACK);
		frmLaboratoire.setBounds(100, 100, 660, 391);
		frmLaboratoire.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmLaboratoire.getContentPane().setLayout(null);
		
		JLabel lblQuelTypeDe = new JLabel("Laboratoire 2");
		lblQuelTypeDe.setBounds(21, 8, 392, 26);
		lblQuelTypeDe.setAlignmentY(Component.TOP_ALIGNMENT);
		frmLaboratoire.getContentPane().add(lblQuelTypeDe);
		
		JComboBox Recherche_Combobox = new JComboBox();
		Recherche_Combobox.setFont(new Font("Tahoma", Font.PLAIN, 14));
		Recherche_Combobox.setModel(new DefaultComboBoxModel(new String[] {"Recherche Binaire", "Recherche Sequentiel", "Recherche Optimise", "Table de Hachage", "Arbre de Recherche"}));
		Recherche_Combobox.setMaximumRowCount(6);
		Recherche_Combobox.setBounds(21, 228, 209, 32);
		frmLaboratoire.getContentPane().add(Recherche_Combobox);
		
		JLabel lblQuelleTypeDe = new JLabel("Quelle type de recherche Utiliser?:");
		lblQuelleTypeDe.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblQuelleTypeDe.setBounds(21, 197, 330, 26);
		frmLaboratoire.getContentPane().add(lblQuelleTypeDe);
		
		txtN = new JTextField();
		txtN.setBounds(300, 87, 51, 32);
		frmLaboratoire.getContentPane().add(txtN);
		txtN.setColumns(10);
		
		txtD = new JTextField();
		txtD.setBounds(300, 159, 51, 32);
		frmLaboratoire.getContentPane().add(txtD);
		txtD.setColumns(10);
		
		JLabel lblNombreDlments = new JLabel("Nombre d'\u00E9l\u00E9ments (1 \u00E0 100)");
		lblNombreDlments.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNombreDlments.setBounds(304, 55, 289, 26);
		frmLaboratoire.getContentPane().add(lblNombreDlments);
		
		JLabel lblDegrDeDsordre = new JLabel("Degr\u00E9 de d\u00E9sordre(0 \u00E0 100)");
		lblDegrDeDsordre.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblDegrDeDsordre.setBounds(304, 123, 289, 26);
		frmLaboratoire.getContentPane().add(lblDegrDeDsordre);
		
		txtR = new JTextField();
		txtR.setBounds(300, 228, 51, 32);
		frmLaboratoire.getContentPane().add(txtR);
		txtR.setColumns(10);
		
		JLabel lblRangDesValeurs = new JLabel("Rang des valeurs (2 \u00E0 8)");
		lblRangDesValeurs.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblRangDesValeurs.setBounds(300, 197, 289, 26);
		frmLaboratoire.getContentPane().add(lblRangDesValeurs);
		
		JLabel lblQuelleTypeDinformation = new JLabel("Quelle type d'information Utiliser?:");
		lblQuelleTypeDinformation.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblQuelleTypeDinformation.setBounds(21, 53, 225, 26);
		frmLaboratoire.getContentPane().add(lblQuelleTypeDinformation);
		
		JComboBox Mode_ComboBox = new JComboBox();
		Mode_ComboBox.setModel(new DefaultComboBoxModel(new String[] {"Generation Aleatoire", "Fichers de donnees "}));
		Mode_ComboBox.setFont(new Font("Tahoma", Font.PLAIN, 15));
		Mode_ComboBox.setBounds(21, 87, 209, 32);
		frmLaboratoire.getContentPane().add(Mode_ComboBox);
		
		JComboBox Tri_Combobox = new JComboBox();
		Tri_Combobox.setFont(new Font("Tahoma", Font.PLAIN, 15));
		Tri_Combobox.setModel(new DefaultComboBoxModel(new String[] {"Aucun Tri", "Insertion", "Pigeonnier", "Par Tas", "Par base", "Rapide", "Fusion"}));
		Tri_Combobox.setBounds(21, 162, 209, 26);
		frmLaboratoire.getContentPane().add(Tri_Combobox);
		
		JLabel lblQuelleTypeDe_1 = new JLabel("Quelle type de tri Utiliser?:");
		lblQuelleTypeDe_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblQuelleTypeDe_1.setBounds(21, 126, 330, 26);
		frmLaboratoire.getContentPane().add(lblQuelleTypeDe_1);
		
		JButton btnResults = new JButton("G\u00E9n\u00E9rer le r\u00E9sultats");
		btnResults.setBounds(21, 292, 264, 35);
		frmLaboratoire.getContentPane().add(btnResults);
		
		JLabel lblNombreDeValeurs = new JLabel("Nombre de valeurs chercher");
		lblNombreDeValeurs.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNombreDeValeurs.setBounds(300, 264, 289, 26);
		frmLaboratoire.getContentPane().add(lblNombreDeValeurs);
		
		txtNbSearched = new JTextField();
		txtNbSearched.setColumns(10);
		txtNbSearched.setBounds(300, 296, 51, 32);
		frmLaboratoire.getContentPane().add(txtNbSearched);
		
		btnResults.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				int Choix_Mode = 1;
				int Choix_Recherche = 1;
				int Choix_Tri = 1;
				int N = 1;
				int R=2;
				int D=0;
				int Nbr_Search = 1;
				
			if(Mode_ComboBox.getSelectedItem().toString() ==  "Generation Aleatoire")
				{
				Choix_Mode = 1;
				}
			else
				{
				Choix_Mode = 2;
				}
			
			switch (Recherche_Combobox.getSelectedItem().toString())
			{			
			case "Recherche Sequentiel":
				Stats.Search_Results.SearchName = "Recherche Sequentiel";
				Choix_Recherche = 2;
				break;	
			case "Recherche Binaire":
				Stats.Search_Results.SearchName = "Recherche Binaire";
				Choix_Recherche = 1;
				break;	
			case "Recherche Optimise":
				Stats.Search_Results.SearchName = "Recherche Optimise";
				Choix_Recherche = 3;
				break;	
			case "Table de Hachage":
				Stats.Search_Results.SearchName = "Recherche Hachage";
				Choix_Recherche = 4;
				break;	
			case "Arbre de Recherche":
				Stats.Search_Results.SearchName = "Arbre de Recherche";
				Choix_Recherche = 5;
				break;	
			}
			
			switch (Tri_Combobox.getSelectedItem().toString())
			{
			case "Aucun Tri":
				Choix_Tri = 7;
				break;	
			case "Insertion":
				Stats.Search_Results.TriName = "Tri par Insertion";
				Choix_Tri = 1;
				break;	
			case "Pigeonnier":
				Stats.Search_Results.TriName = "Tri par Pigeonnier";
				Choix_Tri = 2;
				break;
			case "Rapide":
				Stats.Search_Results.TriName = "Tri par Rapide";
				Choix_Tri = 3;
				break;	
			case "Fusion":
				Stats.Search_Results.TriName = "Tri par Fusion";
				Choix_Tri = 4;
				break;	
			case "Par Tas":
				Stats.Search_Results.TriName = "Tri par Tas";
				Choix_Tri = 5;
				break;	
			case "Par base":
				Stats.Search_Results.TriName = "Tri par base";
				Choix_Tri = 6;
				break;			

			}
			
			try{
				N = Integer.parseInt(txtN.getText());
				Stats.Search_Results.N = N * 1000;
			}
			catch(Exception e)
			{
				N=1;
				Stats.Search_Results.N = N * 1000;
			}	
			
			try{
				
				R = Integer.parseInt(txtR.getText());;		
			}
			catch(Exception e)
			{
				R=3;
			}
			
			try{
				D = Integer.parseInt(txtD.getText());
				Stats.Search_Results.D = D;
			}
			catch(Exception e)
			{
					D = 0;
					Stats.Search_Results.D = D;
			}
			try{
				Nbr_Search = Integer.parseInt(txtNbSearched.getText());		
			}
			catch(Exception e)
			{
					Nbr_Search = 1000;
			}

			Top.GetResults(Choix_Mode, Choix_Tri, Choix_Recherche, N, R, D, Nbr_Search);
			}
			
		});
	}
}
