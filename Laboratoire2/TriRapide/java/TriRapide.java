// Java program for implementation of QuickSort
// Reference : http://quiz.geeksforgeeks.org/quick-sort/
package TriRapide.java;

import java.util.List;
import java.util.ArrayList;
import Stats.java.Stats; 




public class TriRapide
 {
    
    private int array[];
    private int length;
 
    public void sort(int[] inputArr) {
    	Stats.barometre(0);
        if (inputArr == null || inputArr.length == 0) {
        	Stats.barometre(1);
            return;
        }
        this.array = inputArr;
        length = inputArr.length;
        quickSort(0, length - 1);
    }
 
    private void quickSort(int lowerIndex, int higherIndex) {
         
        int i = lowerIndex;
        int j = higherIndex;
        // calculate pivot number, I am taking pivot as middle index number
        int pivot = array[lowerIndex+(higherIndex-lowerIndex)/2];
        // Divide into two arrays
        while (i <= j) {
        	Stats.barometre(2);
            /**
             * In each iteration, we will identify a number from left side which
             * is greater then the pivot value, and also we will identify a number
             * from right side which is less then the pivot value. Once the search
             * is done, then we exchange both numbers.
             */
            while (array[i] < pivot) {
            	Stats.barometre(3);
                i++;
            }
            while (array[j] > pivot) {
            	Stats.barometre(4);
                j--;
            }
            if (i <= j) {
            	Stats.barometre(5);
                exchangeNumbers(i, j);
                //move index to next position on both sides
                i++;
                j--;
            }
        }
        // call quickSort() method recursively
        if (lowerIndex < j){
        	Stats.barometre(6);
            quickSort(lowerIndex, j);
        }
        if (i < higherIndex){
        	Stats.barometre(7);
            quickSort(i, higherIndex);
        }
    }
 
    private void exchangeNumbers(int i, int j) {
    	Stats.barometre(8);
        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    	}

    // Driver program
       public static List<Integer> Quicksort(List<Integer> aArraylist)
    {
       	Stats.barometre(9);
        int []arr = new int [aArraylist.size()];
        
    	for(int i = 0 ; i < arr.length;i++)
    		{
        	Stats.barometre(10);
    	    arr[i] = aArraylist.get(i);
    		}

        List<Integer> Returned_List = new ArrayList<Integer>();
        
        TriRapide ob = new TriRapide();
        ob.sort(arr);

    
        for(int j = 0; j<aArraylist.size(); j++)
		{
        	Stats.barometre(11);
			Returned_List.add(arr[j]);
		}

		return  Returned_List;
    }
}
//This code is contributed by Rajat Mishra 