//   reference : http://www.sanfoundry.com/java-program-implement-avl-tree/
package AvlTree.java;

import java.util.*;

 

 /* Class AVLNode */

 class AVLNode

 {    

     AVLNode left, right;

     int data;

     int height;

 

     /* Constructor w*/

     public AVLNode()

     {

         left = null;

         right = null;

         data = 0;

         height = 0;

     }

     /* Constructor */

     public AVLNode(int n)

     {

         left = null;

         right = null;

         data = n;

         height = 0;

     }     

 }

 

 /* Class AVLTree */

 public class AVLTree

 {

     private AVLNode root;     

 

     /* Constructor */

     public AVLTree()

     {

         root = null;

     }

     /* Function to check if tree is empty */

     public boolean isEmpty()

     {

         return root == null;

     }

     /* Make the tree logically empty */

     public void makeEmpty()

     {

         root = null;

     }

     /* Function to insert data */

     public void insert(int data)

     {

         root = insert(data, root);

     }

     /* Function to get height of node */

     private int height(AVLNode t )

     {

         return t == null ? -1 : t.height;

     }

     /* Function to max of left/right node */

     private int max(int lhs, int rhs)

     {

         return lhs > rhs ? lhs : rhs;

     }

     /* Function to insert data recursively */

     private AVLNode insert(int x, AVLNode t)

     {

         if (t == null)

             t = new AVLNode(x);

         else if (x < t.data)

         {

             t.left = insert( x, t.left );

             if( height( t.left ) - height( t.right ) == 2 )

                 if( x < t.left.data )

                     t = rotateWithLeftChild( t );

                 else

                     t = doubleWithLeftChild( t );

         }

         else if( x > t.data )

         {

             t.right = insert( x, t.right );

             if( height( t.right ) - height( t.left ) == 2 )

                 if( x > t.right.data)

                     t = rotateWithRightChild( t );

                 else

                     t = doubleWithRightChild( t );

         }

         else

           ;  // Duplicate; do nothing

         t.height = max( height( t.left ), height( t.right ) ) + 1;

         return t;

     }

     /* Rotate binary tree node with left child */     

     private AVLNode rotateWithLeftChild(AVLNode k2)

     {

         AVLNode k1 = k2.left;

         k2.left = k1.right;

         k1.right = k2;

         k2.height = max( height( k2.left ), height( k2.right ) ) + 1;

         k1.height = max( height( k1.left ), k2.height ) + 1;

         return k1;

     }

 

     /* Rotate binary tree node with right child */

     private AVLNode rotateWithRightChild(AVLNode k1)

     {

         AVLNode k2 = k1.right;

         k1.right = k2.left;

         k2.left = k1;

         k1.height = max( height( k1.left ), height( k1.right ) ) + 1;

         k2.height = max( height( k2.right ), k1.height ) + 1;

         return k2;

     }

     /**

      * Double rotate binary tree node: first left child

      * with its right child; then node k3 with new left child */

     private AVLNode doubleWithLeftChild(AVLNode k3)

     {

         k3.left = rotateWithRightChild( k3.left );

         return rotateWithLeftChild( k3 );

     }

     /**

      * Double rotate binary tree node: first right child

      * with its left child; then node k1 with new right child */      

     private AVLNode doubleWithRightChild(AVLNode k1)

     {

         k1.right = rotateWithLeftChild( k1.right );

         return rotateWithRightChild( k1 );

     }    

     /* Functions to count number of nodes */

     public int countNodes()

     {

         return countNodes(root);

     }

     private int countNodes(AVLNode r)

     {

         if (r == null)

             return 0;

         else

         {

             int l = 1;

             l += countNodes(r.left);

             l += countNodes(r.right);

             return l;

         }

     }

     /* Functions to search for an element */

     public boolean search(int val)

     {

         return search(root, val);

     }

     private boolean search(AVLNode r, int val)

     {

         boolean found = false;

         while ((r != null) && !found)

         {

             int rval = r.data;

             if (val < rval)

                 r = r.left;

             else if (val > rval)

                 r = r.right;

             else

             {

                 found = true;

                 break;

             }

             found = search(r, val);

         }

         return found;

     }

     /* Function for inorder traversal */

     public void inorder()

     {

         inorder(root);

     }

     private void inorder(AVLNode r)

     {

         if (r != null)

         {

             inorder(r.left);

             System.out.print(r.data +" ");

             inorder(r.right);

         }

     }

     /* Function for preorder traversal */

     public void preorder()

     {

         preorder(root);

     }

     private void preorder(AVLNode r)

     {

         if (r != null)

         {

             System.out.print(r.data +" ");

             preorder(r.left);             

             preorder(r.right);

         }

     }

     /* Function for postorder traversal */

     public void postorder()

     {

         postorder(root);

     }

     private void postorder(AVLNode r)

     {

         if (r != null)

         {

             postorder(r.left);             

             postorder(r.right);

             System.out.print(r.data +" ");

         }

     }     

 
     public static void search(List<Integer> aTableau, List<Integer> aSearchList) {            

        AVLTree avlt = new AVLTree(); 
     }
 }


/*public class BinaryTree {

	Node root;

	public void addNode(int key) {
		Stats.Iteration_Counter = 0;
		Stats.Start_Time();
		
		// Create a new Node and initialize it
		Node newNode = new Node(key);

		// If there is no root this becomes root

		if (root == null) {

			root = newNode;

		} else {

			// Set root as the Node we will start
			// with as we traverse the tree

			Node focusNode = root;

			// Future parent for our new Node

			Node parent;

			while (true) {

				// root is the top parent so we start
				// there

				parent = focusNode;

				// Check if the new node should go on
				// the left side of the parent node

				if (key < focusNode.key) {

					// Switch focus to the left child

					focusNode = focusNode.leftChild;

					// If the left child has no children

					if (focusNode == null) {

						// then place the new node on the left of it

						parent.leftChild = newNode;
						return; // All Done

					}

				} else { // If we get here put the node on the right

					focusNode = focusNode.rightChild;

					// If the right child has no children

					if (focusNode == null) {

						// then place the new node on the right of it

						parent.rightChild = newNode;
						return; // All Done

					}

				}

			}
		}
			Stats.End_Time();
			Stats.Calcule_TR();
			Stats.Calcule_TP();
			Stats.Calcule_TA();

	}

	// All nodes are visited in ascending order
	// Recursion is used to go to one node and
	// then go to its child nodes and so forth

	public void inOrderTraverseTree(Node focusNode) {

		if (focusNode != null) {

			// Traverse the left node

			inOrderTraverseTree(focusNode.leftChild);

			// Visit the currently focused on node

			System.out.println(focusNode);

			// Traverse the right node

			inOrderTraverseTree(focusNode.rightChild);

		}

	}

	public void preorderTraverseTree(Node focusNode) {

		if (focusNode != null) {

			System.out.println(focusNode);

			preorderTraverseTree(focusNode.leftChild);
			preorderTraverseTree(focusNode.rightChild);

		}

	}

	public void postOrderTraverseTree(Node focusNode) {

		if (focusNode != null) {

			postOrderTraverseTree(focusNode.leftChild);
			postOrderTraverseTree(focusNode.rightChild);

			System.out.println(focusNode);

		}
//jjdhfuisdhbufbsid
	}

	public void findNode(int key) {

		// Start at the top of the tree
		
		
		Node focusNode = root;

		// While we haven't found the Node
		// keep looking

		while (focusNode.key != key) {
			Stats.Iteration_Counter = Stats.Iteration_Counter + 1;
			// If we should search to the left

			if (key < focusNode.key) {

				// Shift the focus Node to the left child

				focusNode = focusNode.leftChild;

			} else {

				// Shift the focus Node to the right child

				focusNode = focusNode.rightChild;

			}

			// The node wasn't found

			if (focusNode == null)
				Stats.Search_Results.Iteration.add(-1);

		}

		

	}

public static void search(List<Integer> aTableau, List<Integer> aSearchList) {

		BinaryTree theTree = new BinaryTree();

		for(int i = 0; i < aTableau.size(); i++) {
			theTree.addNode(aTableau.get(i));
		}
		
		for(int i = 0; i < aSearchList.size(); i++) {
			theTree.findNode(aSearchList.get(i));
		}

		// Different ways to traverse binary trees

		// theTree.inOrderTraverseTree(theTree.root);

		// theTree.preorderTraverseTree(theTree.root);

		// theTree.postOrderTraverseTree(theTree.root);

		// Find the node with key 75

		//System.out.println("\nNode with the key 75");

		//System.out.println(theTree.findNode(75));

}
}

class Node {

	int key;

	Node leftChild;
	Node rightChild;

	Node(int key) {

		this.key = key;
		

	}
}*/
	

